/* MESES DEL AÑO */
function Array_Meses(){
    var N=1;
    var meses = new Array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
    alert("MESES DEL AÑO");
    for(var i=0;i<12;i++){
        alert("Mes " + N + ": "+meses[i]);
        N++;
    }
}

/* PRODUCTOS ALIMENTICIOS */
function Array_Producto(){
    function Producto_Alimenticio(código,nombre,precio){
        this.código=código;
        this.nombre=nombre;
        this.precio=precio;
        
    }
    var Producto_1 = new Producto_Alimenticio("00125", "Cola", 1.00);
    var Producto_2 = new Producto_Alimenticio("00153", "Brocoli", 0.80);
    var Producto_3 = new Producto_Alimenticio("00271", "Lechuga", 0.60);

    var Productos =new Array(Producto_1,Producto_2,Producto_3);

    imprimeDatos(Productos);
}

function imprimeDatos(Productos){
    var N=1;
    alert("PRODUCTOS ALIMENTICIOS")
    for(var i=0;i<3;i++){
        alert("Producto " + N + ":  *Código: " + Productos[i].código + "  *Nombre: " + Productos[i].nombre + "  *Precio: $"+Productos[i].precio)
        N++;
    }
}